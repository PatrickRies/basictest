# Basic NodeJS Server

Foobar is a Python library for dealing with word pluralization.

## Installation

```bash
node hello.js
```


## License
[MIT](https://choosealicense.com/licenses/mit/)